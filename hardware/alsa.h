#pragma once

#include <QObject>
#include <QQueue>
#include <QMutex>

#include "global.h"
#include "interfaces/hardware.h"
typedef struct _snd_pcm snd_pcm_t;

class Alsa : public QObject, public Hardware {
    Q_OBJECT

    // Configuration
    int m_sample_frequency;
    size_t m_buffer_size;

    // Interface
public:

    // Thread safe, singleton class
    static Alsa* singleton() {
        static QMutex mutex;
        static Alsa* instance  = 0;

        mutex.lock();
        if(instance == 0)
            instance = new Alsa();

        mutex.unlock();

        return instance;
    }
    void playTone(int t, unsigned int ms = 50);


    /* The filter does NOT free the memory.
     * the filter MAY change the data. */
    typedef void (Filter::*ReceiveCallback)(Amplitude* data, size_t length);

    /* See Protocol::set_application_callback */
    void set_filter_callback(Filter* instance, ReceiveCallback cb);

    ~Alsa();

private:
    // Structs
    struct Job {
        float* buffer;
        unsigned int samples;
        unsigned int loops;
    };

    // Mutex's aka. locks
    QMutex m_workMutex;

    // Variables
    float** m_buffer;
    QQueue<Job> m_queue;
    bool m_initialized;
    bool m_waitingToStopWork;

    // Alsa variables
    snd_pcm_t* m_handle;

    // Functions
protected:
    Alsa() : m_initialized(false),m_waitingToStopWork(false) { init(); }

private:
    bool init();
    void generate_buffers();
    void send_error(QString error);

public slots:
    void work_loop();
};


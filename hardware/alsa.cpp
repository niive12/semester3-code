#include "alsa.h"
#include "string.h"
#include <cmath>
#include "global.h"
#include <alsa/asoundlib.h>
#include <QDebug>


void Alsa::playTone(int t, unsigned int duration)
{
    Job job;

    if(duration < 10){
        duration = 10;
        error("Duration has to be minimum 10ms");
    }

    job.buffer = m_buffer[t];
    job.loops = duration/1000;
    unsigned int rem = (duration - job.loops*1000);
    if(rem > 0)
        job.samples = m_sample_frequency/(1000.0/(duration - job.loops*1000));
    else
        job.samples = 0;

    if(m_workMutex.tryLock(1000)) {
        m_queue.enqueue(job);
        m_workMutex.unlock();
    }else{
        emit error("Queue Mutex timeout, a job has been lost");
    }
}

bool Alsa::init()
{
    if(m_initialized == true) {
        error("Already initialized. Call aborted.");
        return false;
    }

    int err;
    snd_pcm_hw_params_t* params;

    // Some values
    m_sample_frequency = 48000;
    m_buffer_size = m_sample_frequency;

    // Allocate structures
    snd_pcm_hw_params_alloca(&params);

    // Open default device
    err = snd_pcm_open(&m_handle, "default", SND_PCM_STREAM_PLAYBACK, 0);
    if (err < 0) {
        snd_pcm_hw_params_free(params);
        error(QString("Playback open error: %1").arg(snd_strerror(err)));
        return false;
    }
    err = snd_pcm_set_params(m_handle,
                                  SND_PCM_FORMAT_FLOAT,
                                  SND_PCM_ACCESS_RW_INTERLEAVED,
                                  1,
                                  m_sample_frequency,
                                  1,
                                  1000000);
    if(err < 0) {
        // snd_pcm_hw_params_free(params); //Apparently param's should be free'd so this is outcommented
        snd_pcm_close(m_handle);
        error(QString("Playback open error: %1").arg(snd_strerror(err)));
        return false;
    }


    // Generate buffers, only if initialization went ok, so we know we can delete them again
    generate_buffers();

    m_initialized = true;
    return true;
}

Alsa::~Alsa() {
    m_workMutex.lock();
    if(m_initialized == true)  {
        m_waitingToStopWork = true;
        m_workMutex.unlock();

        while(true) {
            usleep(20000);
            if(m_workMutex.tryLock(1000) == false) {
                qDebug() << "Waiting for work to stop...";
            } else {
                m_workMutex.unlock();
                if(m_waitingToStopWork == false)
                    break;
            }
        }

        snd_pcm_close(m_handle);

        for(int i = 0; i < 16; ++i) {
            delete[] m_buffer[i];
        }
        delete[] m_buffer;
    }
    m_workMutex.unlock();
    exit(0);
}

void Alsa::generate_buffers()
{
    int b = 0;
    m_buffer = new float*[1+16]; // Zero buffer + 16 tone combinations
    m_buffer[b] = new float[m_buffer_size]();
    for(int c = 0; c < 4; ++c) {
        for(int r = 0; r < 4; ++r) {

            float* tmp = new float[m_buffer_size];
            int f1 = tones[0][c];
            int f2 = tones[1][r];

            for (size_t k=0; k<m_buffer_size; k++)
                tmp[k] = sin(2*M_PI*f1/m_sample_frequency*k) + sin(2*M_PI*f2/m_sample_frequency*k);

            m_buffer[++b] = tmp;

        }
    }
}


void Alsa::work_loop()
{
    Job job;

    int return_code;
    while(true) {
        static QMutex myMutex;
        if(m_workMutex.tryLock()) {
            if(m_waitingToStopWork) {
                m_waitingToStopWork = false;
                m_workMutex.unlock();
                return;
            }

            if(m_queue.isEmpty() == false) {
                job = m_queue.dequeue();
            }else{
                job.buffer = m_buffer[0];
                job.samples = 5;
                job.loops = 0;
            }
            m_workMutex.unlock();
        }else{
            job.buffer = m_buffer[0];
            job.samples = 5;
            job.loops = 0;
        }
        return_code = 0;
        while(job.loops > 0) {
            return_code = snd_pcm_writei(m_handle, job.buffer, m_buffer_size);
            --job.loops;
        }
        if(return_code >= 0 && job.samples > 0)
            return_code = snd_pcm_writei(m_handle, job.buffer, job.samples);
        if(return_code < 0)
            qDebug() << QString("snd_pcm_writei error %1: %2").arg(return_code).arg(snd_strerror(return_code));
    }
}


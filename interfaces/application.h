#pragma once

#include "global.h"

class Application
{
    friend class Protocol;
public:
    virtual void feed(const char* data, size_t size) = 0;
    virtual void set_protocol(Protocol* instance) {m_protocol = instance;}
protected:
    Protocol* m_protocol;
};

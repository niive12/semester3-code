#pragma once

#include "global.h"
class Transform
{
public:
    void set_protocol(Protocol* instance) { m_protocol = instance; }

    virtual void send_data(const DataTransmission data)=0;
protected:
    Protocol* m_protocol;
private:
    /* Called by the filter layer */
    void feed(const char* data, size_t length);
};

#pragma once

#include "global.h"

class Hardware
{
public:
    virtual ~Hardware() {}

    virtual void playTone(int t, unsigned int ms = 1000) = 0;

    /* See Protocol::set_application_callback */
    void set_filter(Filter* instance) { m_filter = instance; }

protected:

    Filter* m_filter;
};

#pragma once

#include "global.h"

class Protocol
{
    friend class Transform;

public:
    virtual ~Protocol() {}

    virtual void send_message(int addr, const String data)=0;

    /* Usage: Call with an object and a pointer to a member function of that object.
     * Note: Might have been more "correct" to use std::tr1::function
     * to encapsulate, but there is a limit..
     */
    virtual void set_application(Application* instance) { m_application = instance; }
    virtual void set_hardware(Hardware* instance) { m_hardware = instance; }

protected:
    Application* m_application;
    Hardware* m_hardware;

private:
    /* Called by the transform layer */
    virtual void feed(const char* data, size_t length) = 0;


};

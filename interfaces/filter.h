#pragma once

#include "global.h"

class Filter
{
public:
    /* See Protocol::set_application_callback */
    void set_transform(Transform* instance) {m_transform = instance;}
protected:
    Transform* m_transform;
private:
    /* Called by the hardware abstraction layer */
    void feed(const char* data, size_t length);
};

#pragma once
#include "global.h"
#include <QObject>
#include "interfaces/application.h"

class Alsa;

class QThread;

class InstantMessenger : public QObject, public Application
{
    Q_OBJECT

public:

    InstantMessenger(QObject* parent = 0);
    ~InstantMessenger();
    void send_message(const String msg, unsigned int duration, unsigned int pause);
    void feed(const char *data, size_t size);
public slots:
    void playTone(const char* t, unsigned int duration);
};

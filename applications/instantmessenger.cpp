#include "instantmessenger.h"
#include "global.h"

#include <QThread>
#include "interfaces/protocol.h"
#include <QDebug>

InstantMessenger::InstantMessenger(QObject *parent)
    :QObject(parent)
{
}

InstantMessenger::~InstantMessenger()
{
}

void InstantMessenger::send_message(const String msg, unsigned int duration, unsigned int pause) {
    m_protocol->send_message(0, msg);
    info("Ignored duration, pause");
}

void InstantMessenger::feed(const char *data, size_t size)
{
    info(QString::fromAscii(data, size));
}

void InstantMessenger::playTone(const char *c, unsigned int duration)
{
    for(size_t i = 0; i < 16; ++i) {
        if(symbols[i] == *c)
            m_protocol->send_message(0, c);
    }
}


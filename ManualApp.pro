#-------------------------------------------------
#
# Project created by QtCreator 2013-09-30T03:42:57
#
#-------------------------------------------------

QT       += core gui widgets
LIBS += -lasound
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ManualApp
TEMPLATE = app


SOURCES +=  main.cpp \
            global.cpp \
            gui/mainwindow.cpp \
            applications/instantmessenger.cpp \
            hardware/alsa.cpp \
            protocols/simplepro.cpp \
            transforms/fft.cpp \
    buffer/buffer.cpp

HEADERS  += global.h \
            gui/mainwindow.h \
            gui/ui_mainwindow.h \
            interfaces/application.h \
            interfaces/protocol.h \
            interfaces/transform.h \
            interfaces/filter.h \
            interfaces/hardware.h \
            applications/instantmessenger.h \
            protocols/simplepro.h \
            transforms/fft.h \
            hardware/alsa.h

FORMS    += gui/mainwindow.ui

OTHER_FILES += \
    ../notes.txt

HEADERS += \
    filters/NoFilter.h



#pragma once
#include "interfaces/transform.h"
#include "global.h"

class Goertzel : public Transform {
public:
	static double goertzel(double, const int, unsigned);
	void send_data(const DataTransmission data);
};
#include "transforms/fft.h"
#include "Goertzel.h"

//Goertzel transform function
//Returns double(magnitude) of input frequency coefficient.
//"Coeff" is 2*Cos(2*pi*k/N), where k = N*f/fs (rounded to int. Calculate beforehand).
//"samples[]" is an array of sample amplitudes. It's length is "sampleN".
//"Magnitude" is for low frequency.

//Also taken as input is a pointer to Magnitude.
//The Magnitude value itself (what the pointer points to) is changed in the goertzel function.
//So basically you are giving goertzel the address to store the result in.
static double Goertzel::goertzel(double Coeff, const int samples[], unsigned sampleN)
{
	double vk, vk_1=0, vk_2=0;
	//vk_1 = vk(n-1)
	//vk_2 = vk(n-2)

	//Calculate vk "sampleN" times:
	for(unsigned i=0; i<sampleN; ++i)
	{
		//See page 381 in Li Tan if you don't know what's going on here.
		//vk is vk(n). n is samples[i].
		vk=Coeff*vk_1 - vk_2 + double(samples[i]);
		vk_2=vk_1;
		vk_1=vk;
	}

	//Calculate magnitude (|Xk|^2):
	//See page 381 in Li Tan if you don't know what's going on here.
	return vk_1*vk_1 + vk_2*vk_2 - Coeff*vk_1*vk_2;
}


//I don't know what this is yet:
void Goertzel::send_data(const DataTransmission data)
{
}
#pragma once
#include "interfaces/transform.h"
#include "global.h"

class Filter;
class FFT : public Transform {
public:
    FFT() {};
    void send_data(const DataTransmission data);
};

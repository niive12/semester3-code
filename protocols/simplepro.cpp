#include "simplepro.h"
#include "interfaces/application.h"
#include "interfaces/hardware.h"
void SimpleProtocol::feed(const char *data, size_t length)
{
    m_application->feed(data, length);
}


void SimpleProtocol::send_message(int addr, const String data)
{
    for(size_t x = 0; x < data.length; ++x) {
        for(size_t i = 0; i < 16; ++i) {
            if(symbols[i] == data.data[x]) {
                /* Play a tone*/
                m_hardware->playTone(i+1);
                /* Create a pause */
                m_hardware->playTone(0);
                break;
            }
        }
        debugn("Unknown symbol %c", msg[x]);
    }
}

/* Create header
 * add to string
 * send to buffer */

/* Sending:
 * Create Header
 * Sync byte should be extra long
 * Time slots should begin after sync byte stops.
 *
 * suggestion: different tones for different purposes. (it is limitting but quick)
 *
 * All time:
 * Listen for collision and determine if this is a good time to send
 * Toggle a boolean so every layer can use the information: silent or data.
 *
 * Recieve:
 * extract header
 * send to application
 */

#pragma once

#include "interfaces/protocol.h"

class SimpleProtocol : public Protocol {

public:
    void send_message(int addr, const String);

private:
    virtual void feed(const char* data, size_t length);
};

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}
class InstantMessenger;
class QThread;
class Alsa;
class Protocol;
class Transform;
class Filter;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    QThread* m_work_thread;
    Alsa* m_hardware;
    Protocol* m_protocol;
    Transform* m_transform;
    Filter* m_filter;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void displayError(QString msg);
    void displayInfo(QString msg);

    void send();
    void keypad_1();
    void keypad_2();
    void keypad_3();
    void keypad_4();
    void keypad_5();
    void keypad_6();
    void keypad_7();
    void keypad_8();
    void keypad_9();
    void keypad_0();
    void keypad_s();
    void keypad_f();
    void keypad_A();
    void keypad_B();
    void keypad_C();
    void keypad_D();
private:
    Ui::MainWindow *ui;
    InstantMessenger* m_application;
};

#endif // MAINWINDOW_H

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "applications/instantmessenger.h"
#include <QThread>
#include "hardware/alsa.h"
#include "protocols/simplepro.h"
#include "transforms/fft.h"
#include "filters/NoFilter.h"
#include <QDebug>
/* Error message handler */
static MainWindow* message_receiver = 0;
void route_message(int p, const QString msg) {
    if(message_receiver != 0) {
        if(p == 0)
            message_receiver->displayInfo(msg);
        else if(p==1)
            message_receiver->displayError(msg);

    }
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    /* Use the userinterface defined in mainwindow.ui */
    ui->setupUi(this);

    /* Set error handler */
    message_receiver = this;
    setMessageHandler(route_message);

    /* Creater layers */
    m_hardware = Alsa::singleton();
    m_filter = new NoFilter();
    m_transform = new FFT();
    m_protocol = new SimpleProtocol();
    m_application = new InstantMessenger();

    /* Bind layers together */
    m_filter->set_transform(m_transform);
    m_transform->set_protocol(m_protocol);
    m_protocol->set_application(m_application);
    m_protocol->set_hardware(m_hardware);
    m_transform->set_protocol(m_protocol);
    m_application->set_protocol(m_protocol);

    /* Move hardware layer to seperate cpu thread, and start the work loop */
    m_work_thread = new QThread(this);
    connect(m_work_thread, SIGNAL(started()), m_hardware, SLOT(work_loop()));
    m_hardware->moveToThread(m_work_thread);
    m_work_thread->start();

    /* Configure UI */
    showMaximized();

    ui->splitter->setSizes(QList<int>() << 200 << 100000);
    ui->splitter_2->setSizes(QList<int>() << 200 << 100000);

    connect(ui->send, SIGNAL(clicked()), this, SLOT(send()));
    connect(ui->pb_1, SIGNAL(clicked()), this, SLOT(keypad_1()));
    connect(ui->pb_2, SIGNAL(clicked()), this, SLOT(keypad_2()));
    connect(ui->pb_3, SIGNAL(clicked()), this, SLOT(keypad_3()));
    connect(ui->pb_4, SIGNAL(clicked()), this, SLOT(keypad_4()));
    connect(ui->pb_5, SIGNAL(clicked()), this, SLOT(keypad_5()));
    connect(ui->pb_6, SIGNAL(clicked()), this, SLOT(keypad_6()));
    connect(ui->pb_7, SIGNAL(clicked()), this, SLOT(keypad_7()));
    connect(ui->pb_8, SIGNAL(clicked()), this, SLOT(keypad_8()));
    connect(ui->pb_9, SIGNAL(clicked()), this, SLOT(keypad_9()));
    connect(ui->pb_0, SIGNAL(clicked()), this, SLOT(keypad_0()));
    connect(ui->pb_s, SIGNAL(clicked()), this, SLOT(keypad_s()));
    connect(ui->pb_f, SIGNAL(clicked()), this, SLOT(keypad_f()));
    connect(ui->pb_A, SIGNAL(clicked()), this, SLOT(keypad_A()));
    connect(ui->pb_B, SIGNAL(clicked()), this, SLOT(keypad_B()));
    connect(ui->pb_C, SIGNAL(clicked()), this, SLOT(keypad_C()));
    connect(ui->pb_D, SIGNAL(clicked()), this, SLOT(keypad_D()));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_application;
    delete m_hardware; // AlsaConnection's destructor is thread-safe
    m_work_thread->exit();

}

void MainWindow::displayError(QString msg)
{
    ui->log->append(msg);
}
void MainWindow::displayInfo(QString msg)
{
    ui->log->append(msg);
}

void MainWindow::send()
{
    m_application->send_message(String(ui->message->toPlainText().toAscii()), ui->messageDuration->value(), ui->messagePause->value());
}

void MainWindow::keypad_1() { m_application->playTone("1", ui->keypadDuration->value()); }
void MainWindow::keypad_2() { m_application->playTone("2", ui->keypadDuration->value()); }
void MainWindow::keypad_3() { m_application->playTone("3", ui->keypadDuration->value()); }
void MainWindow::keypad_4() { m_application->playTone("4", ui->keypadDuration->value()); }
void MainWindow::keypad_5() { m_application->playTone("5", ui->keypadDuration->value()); }
void MainWindow::keypad_6() { m_application->playTone("6", ui->keypadDuration->value()); }
void MainWindow::keypad_7() { m_application->playTone("7", ui->keypadDuration->value()); }
void MainWindow::keypad_8() { m_application->playTone("8", ui->keypadDuration->value()); }
void MainWindow::keypad_9() { m_application->playTone("9", ui->keypadDuration->value()); }
void MainWindow::keypad_0() { m_application->playTone("0", ui->keypadDuration->value()); }
void MainWindow::keypad_s() { m_application->playTone("s", ui->keypadDuration->value()); }
void MainWindow::keypad_f() { m_application->playTone("f", ui->keypadDuration->value()); }
void MainWindow::keypad_A() { m_application->playTone("A", ui->keypadDuration->value()); }
void MainWindow::keypad_B() { m_application->playTone("B", ui->keypadDuration->value()); }
void MainWindow::keypad_C() { m_application->playTone("C", ui->keypadDuration->value()); }
void MainWindow::keypad_D() { m_application->playTone("D", ui->keypadDuration->value()); }

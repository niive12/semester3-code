#include "global.h"
#include <QString>

MessageCallback message_callback = 0;
void setMessageHandler(MessageCallback cb) {
    message_callback = cb;
}

void message(int priority, const QString msg) {
    if(message_callback != 0)
        (*message_callback)(priority, msg);
}

void info(const QString msg) { message(0, msg); }
void error(const QString msg) { message(1, msg); }

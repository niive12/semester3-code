#pragma once
#include <cstring>

typedef long unsigned int size_t;

const char symbols[] = {'1','2','3','4','5','6','7','8','9','*','0','#','A','B','C','D'};
const int tones[2][4] = {
  {697, 770, 852, 941},
  {1209, 1336, 1477, 1633}
  };

class Hardware;
class Application;
class Protocol;
class Transform;
class Filter;
class Alsa;

typedef float Amplitude;

/* We could use a bit field instead */
struct AmplitudeList {
    Amplitude signal[16];
};

struct String {
    String(const char* chr) { data = chr; length = strlen(chr); }
    const char* data;
    size_t length;
};

struct DataTransmission {
    AmplitudeList* data;
    size_t length;
    size_t mem_size;
    DataTransmission(AmplitudeList* amp, size_t mem_size) : data(amp), mem_size(mem_size) {}
};

//#define PRINTDEBUG
#ifdef PRINTDEBUG
static unsigned int gc = 0;
#define debug(fmt) printf("%-4d:%-4d:%-20s "fmt"\n",++gc, __LINE__, __FUNCTION__);
#define debugn(fmt, args...) printf("%-4d:%-4d:%-20s "fmt"\n",++gc, __LINE__, __FUNCTION__, args);
#else
#define debug(fmt)
#define debugn(fmt, args...)
#endif

class QString;
typedef void (*MessageCallback)(int priority, const QString msg);

void setMessageHandler(MessageCallback cb);
void message(int priority, const QString msg);
void error(const QString msg);
void info(const QString msg);

enum ReturnCode {
    EUNKNOWN = -1,
    SUCCESS = 0
};
